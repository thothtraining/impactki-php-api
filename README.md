ImpactKI-PHP-API
================

API for ImpactKI written in PHP

Check out the Wiki for more information on how to use the API.

(note: You will need an API Auth key from ImpactKI in order to use this API)

We support composer! https://packagist.org/packages/impactki/impactki-php-api

Add this to your composer.json file:
```
"impactki/impactki-php-api": "1.0.*"
```
